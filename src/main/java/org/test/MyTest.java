/**
 *    Copyright ${license.git.copyrightYears} the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.test;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.test.entity.User;
import org.test.mapper.UserMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author lwh
 * @create 2021-09-07 0:24
 */

public class MyTest {
  public static void main(String[] args) throws IOException {
    // 加载核心配置文件到流
    InputStream resourceAsStream = Resources.getResourceAsStream("mybatis-conf.xml");
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
    SqlSession sqlSession = sqlSessionFactory.openSession();
    Object o = sqlSession.selectList("org.test.mapper.UserMapper.selectAll");
    System.out.println(o.toString());

    // 获取代理对象
    UserMapper mapper = sqlSession.getMapper(UserMapper.class);
    // 代理对象调用方法,是调用InvocationHandler接口实现类的invoke方法
    List<User> users = mapper.selectAll();
    System.out.println(users);


  }
}
